import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        createLinkedPurchaseList(session);
        transaction.commit();

        sessionFactory.close();

    }
    public static void createLinkedPurchaseList(Session session) {
        List<PurchaseList> purchaseLists = session.createQuery("from PurchaseList").getResultList();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

        CriteriaQuery<Course> criteriaQueryCourse = criteriaBuilder.createQuery(Course.class);
        CriteriaQuery<Student>criteriaQueryStudents = criteriaBuilder.createQuery(Student.class);

        Root<Course> courseRoot = criteriaQueryCourse.from(Course.class);
        criteriaQueryCourse.select(courseRoot);

        Root<Student> studentRoot = criteriaQueryStudents.from(Student.class);
        criteriaQueryStudents.select(studentRoot);

        for (PurchaseList purchaseList : purchaseLists) {
            criteriaQueryCourse.where(criteriaBuilder.equal(courseRoot.get("name"), purchaseList.getCourseName()));
            Query<Course> queryCourse = session.createQuery(criteriaQueryCourse);
            Course course = queryCourse.list().stream().findFirst().get();

            criteriaQueryStudents.where(criteriaBuilder.equal(studentRoot.get("name"), purchaseList.getStudentName()));
            Query<Student> queryStudent = session.createQuery(criteriaQueryStudents);
            Student student = queryStudent.list().stream().findFirst().get();

            LinkedPurchaseList linkedPurchaseList = new LinkedPurchaseList(new Key(student.getId(), course.getId()), student.getId(), course.getId());
            session.saveOrUpdate(linkedPurchaseList);
        }

    }
}
